<?php

namespace Omnipay\Elavon\Message;

use Omnipay\Common\Message\AbstractResponse;

/**
 * Redsys Response
 */
class CompletePurchaseResponse extends AbstractResponse
{
    public function isSuccessful()
    {
        if (isset($this->data['RESULT']) && $this->data['RESULT'] == 0) {
            return true;
        }
        return false;
    }

    public function isRedirect()
    {
        return false;
    }

    public function getTransactionReference() {
        return $this->data['AUTHCODE'];
    }

    public function getExtraData() {
        return $this->data['MESSAGE'];
    }

}
