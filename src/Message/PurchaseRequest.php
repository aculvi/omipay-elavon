<?php

namespace Omnipay\Elavon\Message;

use Omnipay\Common\Message\AbstractRequest;

/**
 * Redsys Purchase Request
 */
class PurchaseRequest extends AbstractRequest
{
    protected $liveEndpoint = 'https://hpp.santanderelavontpvvirtual.es/pay';

    protected $testEndpoint = 'https://hpp.prueba.santanderelavontpvvirtual.es/pay';

    public function getMerchantId()
    {
        return $this->getParameter('merchantId');
    }

    public function setMerchantId($value)
    {
        return $this->setParameter('merchantId', $value);
    }

    public function getSecretKey()
    {
        return $this->getParameter('secret');
    }

    public function setSecretKey($value)
    {
        return $this->setParameter('secret', $value);
    }

    public function getOrder(){
        return $this->getParameter('order');
    }

    public function setOrder(){
        return $this->setParameter('order',date());
    }

    public function getAccount()
    {
        return $this->getParameter('account');
    }

    public function setAccount($value)
    {
        return $this->setParameter('account', $value);
    }

    public function getLanguage()
    {
        return $this->getParameter('language');
    }

    public function setLanguage($value)
    {
        return $this->setParameter('language', $value);
    }

    public function getExtraData()
    {
        return $this->getParameter('extraData');
    }

    public function setExtraData($value)
    {
        return $this->setParameter('extraData', $value);
    }

    public function getAuthorisationCode()
    {
        return $this->getParameter('authorisationCode');
    }

    public function setAuthorisationCode($value)
    {
        return $this->setParameter('authorisationCode', $value);
    }

    public function getPayMethods()
    {
        return $this->getParameter('payMethods');
    }

    public function setPayMethods($value)
    {
        return $this->setParameter('payMethods', $value);
    }

    public function generateSignature($data) {
        $signature = '';

        foreach (array('TIMESTAMP', 'MERCHANT_ID', 'ORDER_ID', 'AMOUNT', 'CURRENCY') as $field) {
            $signature .= $data[$field];
        }
        $signature = sha1($signature);
        $signature .= $this->getSecretKey();
        $signature = sha1($signature);

        return $signature;
    }

    public function getData()
    {
        $this->validate('amount',
                        'currency',
                        'transactionId',
                        'merchantCode',
                        'terminal');

        $amount = str_replace('.', '', $this->getAmount());
        $timestamp = strftime("%Y%m%d%H%M%S");
        mt_srand((double)microtime()*1000000);
        $order = $timestamp."-".mt_rand(1, 999);
        $card = $this->getCard();

        $data = array(
            'MERCHANT_ID' => $this->getMerchantId(),
            'ORDER_ID' => $order,
            'CURRENCY' => $this->getCurrencyNumeric(),
            'AMOUNT' => $amount,
            'ACCOUNT' => $this->getAccount(),
            'TIMESTAMP' => $this->getMerchantName(),
            'COMMENT1' => $card->getDescription(),
            'AUTO_SETTLE_FLAG' => 1
        );

        if($this->getRedirectTo()) $data['REDIRECT_TO'] = $this->getRedirectTo();

        $data['SHA1HASH'] = $this->generateSignature($data);

        return $data;
    }

    public function sendData($data)
    {
        return $this->response = new PurchaseResponse($this, $data);
    }

    public function getEndpoint()
    {
        return $this->getTestMode() ? $this->testEndpoint : $this->liveEndpoint;
    }

}
