<?php

namespace Omnipay\Elavon\Message;

use Omnipay\Common\Exception\InvalidResponseException;

/**
 * Redsys Complete Purchase Request
 */
class CompletePurchaseRequest extends PurchaseRequest
{

    public function checkSignature($data) {
        if (!isset($data['SHA1HASH'])) {
            return false;
        }

        $signature = '';

        foreach (array('TIMESTAMP', 'MERCHANT_ID', 'ORDER_ID', 'AMOUNT', 'CURRENCY') as $field) {
            $signature .= $data[$field];
        }
        $signature = sha1($signature);
        $signature .= $this->getSecretKey();
        $signature = sha1($signature);

        return $signature == strtolower($data['SHA1HASH']);
    }

    public function getData()
    {
        $query = $this->httpRequest->request;

        $data = array();

        foreach (array('MERCHANT_ID', 'ORDER_ID', 'AMOUNT', 'TIMESTAMP', 'SHA1HASH', 'RESULT', 'MESSAGE', 'PASREF', 'AUTHCODE') as $field) {
            $data[$field] = $query->get($field);
        }

        if (!$this->checkSignature($data)) {
            throw new InvalidResponseException("Invalid signature, Order: {$data['ORDER_ID']}.");
        }

        return $data;
    }

    public function sendData($data)
    {
        return $this->response = new CompletePurchaseResponse($this, $data);
    }
}