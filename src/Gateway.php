<?php

namespace Omnipay\Elavon;

use Omnipay\Common\AbstractGateway;
use Omnipay\Dummy\Message\AuthorizeRequest;

/**
 * Redsys Gateway
 */
class Gateway extends AbstractGateway
{
    public function getName()
    {
        return 'Elavon';
    }

    public function getDefaultParameters()
    {
        return array(
            'merchantId' => '',
            'acount' => '',
            'autoSettleFlag' => 1,
            'testMode' => false,
        );
    }

    public function getMerchantId()
    {
        return $this->getParameter('merchantId');
    }

    public function setMerchantCode($value)
    {
        return $this->setParameter('merchantId', $value);
    }

    public function getSecretKey()
    {
        return $this->getParameter('secret');
    }

    public function setSecretKey($value)
    {
        return $this->setParameter('secret', $value);
    }

    public function getTerminal()
    {
        return $this->getParameter('terminal');
    }

    public function setTerminal($value)
    {
        return $this->setParameter('terminal', $value);
    }

    public function getPayMethods()
    {
        return $this->getParameter('payMethods');
    }

    public function setPayMethods($value)
    {
        return $this->setParameter('payMethods', $value);
    }

    public function purchase(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\Elavon\Message\PurchaseRequest', $parameters);
    }

    public function completePurchase(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\Elavon\Message\CompletePurchaseRequest', $parameters);
    }
}
